package com.homeWork_1;
import java.util.Scanner;
import java.util.Random;

public class Number {
    public static void main(String[] args){
        System.out.println("Let the game begin!");
        System.out.println("Your Name?");
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();

        Random random = new Random();

        String[][] stories = {
                {"1939", "World War 2"},
                {"2014", "Russian-Ukrainian War"}
        };

        int randomStoryIndex = random.nextInt(stories.length);

        //int randomNumber = random.nextInt(0,3);

        int[] userNumbers = new int[100];
        int attempts = 0;
        int correctAnswers = 0;

        while (correctAnswers <= stories.length){
            String story = stories[randomStoryIndex][1];
            int yearGuessed= Integer.parseInt(stories[randomStoryIndex][0]);
            System.out.println("When did the " + story + " begin?");

           while (!scan.hasNextInt()){
               System.out.println("Incorrect");
               scan.next();
               System.out.println("Year?");
           }

            int year =  scan.nextInt();
           userNumbers[attempts] = year;
           attempts++;

            if (year == yearGuessed){
                System.out.println("Correct!");
              correctAnswers ++;
            } else if (year < yearGuessed) {
                System.out.println("Your number is too small. Please, try again.");
            } else {
                System.out.println("Your number is too big. Please, try again.");
            }
        }
        System.out.printf("Congratulations, %s You answered all questions.", name);

        System.out.print("\nYour numbers: ");
        for (int i=0; i<attempts; i++) {
            System.out.print(userNumbers[i]+"\t");
        }
scan.close();
    }
}
